import React, {Component} from 'react'
import {connect} from 'react-redux'
import {addFavCur} from './actions'
import {bindActionCreators} from 'redux'

class RadioElem extends Component{
    constructor(props: any)
    {
        super(props);
        this.state = {
          val: props.valuta
        } 
    }
	render(){

		return(
				<div>
         <form>
		 
		 	<div className="radio">
		 	
		 	
          <label>
            <input type="radio" value= {this.state.val[0]} 
            checked={this.state.selectedOption === 'option3'}
            onClick={ () =>  this.props.addFavCur(this.state.val[0]) }  />
            {this.state.val[0]}
          </label>
        </div>
		 	
	</form>	
				 
    </div>
				)

	}
}

function mapStateToProps(state){  
  return{curs: state}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({addFavCur: addFavCur},dispatch)
}

export default connect (mapStateToProp, mapDispatchToProps ) (RadioElem)

