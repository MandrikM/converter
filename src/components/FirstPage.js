import React, {Component} from 'react'
import {connect} from 'react-redux'
import {addFavCur} from './actions'
import {bindActionCreators} from 'redux'
import RadioElem from "./RadioElem"

class FirstPage extends Component{

	render(){

		return(
				<div>
				{console.log(this.props.curs.allCurrencies)}
				
<form>
		 {this.props.curs.allCurrencies.map((val)=>{return(
	<RadioElem valuta = {val}/>

		 	)})}
	
	</form>			 
    </div>
				)

	}
}

function mapStateToProps(state){	
	return{curs: state}
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({addFavCur: addFavCur},dispatch)
}

export default connect (mapStateToProps, mapDispatchToProps ) (FirstPage)

