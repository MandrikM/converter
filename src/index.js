import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import {Provider} from 'react-redux'
import { createStore, applyMiddleware} from 'redux'
import reducer from './components/reducer'
import createSagaMiddleware from 'redux-saga'
import watchAll from './sagas/watchAll'


const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(watchAll)


function render(){
	ReactDOM.render(
	<Provider store ={store}>
		<App />
	</Provider>,
document.getElementById('root')
	); 
}

render()
store.subscribe(render)


