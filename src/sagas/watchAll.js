import {all} from 'redux-saga/effects'
import showAllCur from "./showAllCur"


export default function* watchAll() {
  yield all([
    
    showAllCur()
  ])
}
