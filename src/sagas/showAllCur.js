import { put, takeEvery} from 'redux-saga/effects'

export default function* showAllCur(){
  yield takeEvery("SHOW ALL CUR", allCur)
}

function* allCur(action){

  const curs = yield fetch("https://mmandrik.free.beeceptor.com/curs")
      .then(response => response.json())

  console.log(curs)
  yield put({ type: "ALL CUR", payload: curs })
}